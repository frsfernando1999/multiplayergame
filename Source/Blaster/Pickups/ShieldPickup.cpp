// Fill out your copyright notice in the Description page of Project Settings.


#include "ShieldPickup.h"

#include "Blaster/BlasterComponents/BuffComponent.h"
#include "Blaster/Character/BlasterCharacter.h"

void AShieldPickup::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                    UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	Super::OnSphereOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	const ABlasterCharacter* Character = Cast<ABlasterCharacter>(OtherActor);

	if (Character && Character->GetCombatComp() && Character->GetBuffComponent())
	{
		const UBuffComponent* Buff = Character->GetBuffComponent();

		Buff->ReplenishShield(ShieldAmount);		
		Destroy();
	}
}
