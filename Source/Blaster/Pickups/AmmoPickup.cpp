// Fill out your copyright notice in the Description page of Project Settings.


#include "AmmoPickup.h"

#include "Blaster/Character/BlasterCharacter.h"

void AAmmoPickup::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                  UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnSphereOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	const ABlasterCharacter* Character = Cast<ABlasterCharacter>(OtherActor);
	if (Character && Character->GetCombatComp())
	{		
		UCombatComponent* Combat = Character->GetCombatComp();
		Combat->PickupAmmo(AmmoType, AmmoAmount);
		Destroy();
	}
}
