// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthPickup.h"

#include "Blaster/BlasterComponents/BuffComponent.h"
#include "Blaster/Character/BlasterCharacter.h"

AHealthPickup::AHealthPickup()
{
	bReplicates = true;
}

void AHealthPickup::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                    UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                    const FHitResult& SweepResult)
{
	Super::OnSphereOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	const ABlasterCharacter* Character = Cast<ABlasterCharacter>(OtherActor);

	if (Character && Character->GetCombatComp() && Character->GetBuffComponent())
	{
		UBuffComponent* Buff = Character->GetBuffComponent();

		Buff->Heal(HealAmount, HealingTime);		
		Destroy();
	}
}
