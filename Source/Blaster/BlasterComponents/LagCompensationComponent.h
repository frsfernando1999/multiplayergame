// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "LagCompensationComponent.generated.h"

USTRUCT(BlueprintType)
struct FBoxInformation
{
	GENERATED_BODY()

	UPROPERTY()
	FVector Location;

	UPROPERTY()
	FRotator Rotation;

	UPROPERTY()
	FVector BoxExtent;
};

USTRUCT(BlueprintType)
struct FFramePackage
{
	GENERATED_BODY()

	UPROPERTY()
	float Time;

	UPROPERTY()
	TMap<FName, FBoxInformation> HitBoxInfo;

	UPROPERTY()
	class ABlasterCharacter* Character;
};

USTRUCT(BlueprintType)
struct FServerSideRewindResult
{
	GENERATED_BODY()

	UPROPERTY()
	bool bHitConfirmed;

	UPROPERTY()
	bool bHeadshot;
};

USTRUCT(BlueprintType)
struct FShotgunServerSideRewindResult
{
	GENERATED_BODY()

	UPROPERTY()
	TMap<class ABlasterCharacter*, int32> HeadShots;

	UPROPERTY()
	TMap<ABlasterCharacter*, int32> BodyShots;
};

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class BLASTER_API ULagCompensationComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	ULagCompensationComponent();
	friend class ABlasterCharacter;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
	                           FActorComponentTickFunction* ThisTickFunction) override;
	void ShowFramePackage(const FFramePackage& Package, const FColor Color) const;

	//Hitscan
	FServerSideRewindResult ServerSideRewindHitScan(
		ABlasterCharacter* HitCharacter,
		const FVector_NetQuantize& TraceStart,
		const FVector_NetQuantize& HitLocation,
		float HitTime) const;

	// Shotgun 
	FShotgunServerSideRewindResult ShotgunServerSideRewind(const TArray<ABlasterCharacter*>& HitCharacters,
	                                                       const FVector_NetQuantize& TraceStart,
	                                                       const TArray<FVector_NetQuantize>& HitLocations,
	                                                       float HitTime) const;
	//Projectile
	FServerSideRewindResult ServerSideRewindProjectile(
		ABlasterCharacter* HitCharacter,
		const FVector_NetQuantize& TraceStart,
		const FVector_NetQuantize100& InitialVelocity,
		float HitTime) const;


	UFUNCTION(Server, Reliable)
	void ServerScoreRequestHitscan(
		ABlasterCharacter* HitCharacter,
		const FVector_NetQuantize& TraceStart,
		const FVector_NetQuantize& HitLocation,
		float HitTime
	);

	UFUNCTION(Server, Reliable)
	void ServerScoreRequestProjectile(
		ABlasterCharacter* HitCharacter,
		const FVector_NetQuantize& TraceStart,
		const FVector_NetQuantize100& InitialVelocity,
		float HitTime
	);

	UFUNCTION(Server, Reliable)
	void ServerScoreRequestShotgun(const TArray<ABlasterCharacter*>& HitCharacters,
	                               const FVector_NetQuantize& TraceStart,
	                               const TArray<FVector_NetQuantize>& HitLocations,
	                               float HitTime);

protected:
	virtual void BeginPlay() override;
	void SaveServerFramePackage();
	void SaveFramePackage(FFramePackage& Package);
	FFramePackage InterpBetweenFrames(const FFramePackage& OlderFrame, const FFramePackage& YoungerFrame,
	                                  float HitTime) const;
	static void CacheBoxPositions(ABlasterCharacter* HitCharacter,
	                              FFramePackage& OutFramePackage);
	static void MoveBoxes(ABlasterCharacter* HitCharacter, const FFramePackage& Package);
	static void ResetHitBoxes(ABlasterCharacter* HitCharacter, const FFramePackage& Package);
	static void EnableCharacterMeshCollision(const ABlasterCharacter* HitCharacter,
	                                         ECollisionEnabled::Type CollisionType);
	FFramePackage GetFrameToCheck(ABlasterCharacter* HitCharacter, float HitTime) const;

	// Hitscan

	FServerSideRewindResult ConfirmHitHitScan(
		const FFramePackage& Package,
		ABlasterCharacter* HitCharacter,
		const FVector_NetQuantize& TraceStart,
		const FVector_NetQuantize& HitLocation) const;

	// Shotgun

	FShotgunServerSideRewindResult ShotgunConfirmHit(const TArray<FFramePackage>& FramePackages,
	                                                 const FVector_NetQuantize& TraceStart,
	                                                 const TArray<FVector_NetQuantize>& HitLocations) const;

	//Projectile
	FServerSideRewindResult ConfirmHitProjectile(
		const FFramePackage& Package,
		ABlasterCharacter* HitCharacter,
		const FVector_NetQuantize& TraceStart,
		const FVector_NetQuantize100& InitialVelocity,
		float HitTime) const;


private:
	UPROPERTY()
	ABlasterCharacter* Character;

	UPROPERTY()
	class ABlasterPlayerController* Controller;

	TDoubleLinkedList<FFramePackage> FrameHistory;

	UPROPERTY(EditAnywhere)
	float MaxRecordTime = 2.f;

public:
};
