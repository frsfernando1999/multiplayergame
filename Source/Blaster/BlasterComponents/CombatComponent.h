// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blaster/BlasterTypes/CombatState.h"
#include "Blaster/HUD/BlasterHUD.h"
#include "Blaster/Weapon/WeaponTypes.h"
#include "Components/ActorComponent.h"
#include "CombatComponent.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class BLASTER_API UCombatComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UCombatComponent();
	friend class ABlasterCharacter;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
	                           FActorComponentTickFunction* ThisTickFunction) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void	EquipWeapon(class AWeapon* WeaponToEquip);
	void SwapWeapons();
	void Reload();

	UFUNCTION(BlueprintCallable)
	void FinishReloading();

	UFUNCTION(BlueprintCallable)
	void FinishSwap();

	UFUNCTION(BlueprintCallable)
	void FinishSwapAttachWeapons();

	void FireButtonPressed(bool bPressed);

	UFUNCTION(BlueprintCallable)
	void ShotgunShellReload();

	UFUNCTION(BlueprintCallable)
	void ThrowGrenadeFinished();

	UFUNCTION(BlueprintCallable)
	void LaunchGrenade();

	UFUNCTION(Server, Reliable)
	void Server_LaunchGrenade(const FVector_NetQuantize Target);

	void JumpToShotgunEnd() const;

	void PickupAmmo(EWeaponType AmmoType, int32 AmmoAmount);

	bool bLocallyReloading = false;

protected:
	virtual void BeginPlay() override;

	void SetAiming(bool bIsAiming);

	UFUNCTION(Server, Reliable)
	void ServerSetAiming(bool bIsAiming);

	UFUNCTION()
	void OnRep_EquippedWeapon() const;
	
	UFUNCTION()
	void OnRep_SecondaryWeapon() const;
	
	void Fire();
	void FireProjectileWeapon();
	void FireHitScanWeapon();
	void FireShotgun();
	
	void LocalFire(const FVector_NetQuantize& TraceHitTarget) const;
	void LocalShotgunFire(const TArray<FVector_NetQuantize>& TraceHitTargets);


	//Calls function to execute on server
	UFUNCTION(Server, Reliable, WithValidation)
	void ServerFire(const FVector_NetQuantize& TraceHitTarget, float FireDelay);

	//Server executes it on all machines
	UFUNCTION(NetMulticast, Reliable)
	void MulticastFire(const FVector_NetQuantize& TraceHitTarget);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerShotgunFire(const TArray<FVector_NetQuantize>& TraceHitTargets, float FireDelay);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastShotgunFire(const TArray<FVector_NetQuantize>& TraceHitTargets);

	void TraceUnderCrosshairs(FHitResult& TraceHitResult);

	void SetHUDCrosshairs(float DeltaTime);

	UFUNCTION(Server, Reliable)
	void ServerReload();

	void HandleReload() const;
	int32 AmountToReload();

	void ThrowGrenade();

	UFUNCTION(Server, Reliable)
	void Server_ThrowGrenade();

	UPROPERTY(EditAnywhere)
	TSubclassOf<class AProjectile> GrenadeClass;

	void DropEquippedWeapon() const;
	void AttachActorToRightHand(AActor* ActorToAttach) const;
	void AttachActorToLeftHand(AActor* ActorToAttach) const;
	void AttachFlagToLeftHand(AWeapon* Flag) const;
	void AttachActorToBackPack(AActor* ActorToAttach) const;
	void UpdateCarriedAmmo();
	void PlayEquipWeaponSound(const AWeapon* Weapon) const;
	void ReloadEmptyWeapon();
	void ShowAttachedGrenade(bool bShowGrenade) const;


	//Always try to use replication instead of RPCs

private:
	UPROPERTY()
	ABlasterCharacter* Character;

	UPROPERTY()
	class ABlasterPlayerController* Controller;

	UPROPERTY()
	ABlasterHUD* HUD;

	UPROPERTY(ReplicatedUsing = OnRep_EquippedWeapon)
	AWeapon* EquippedWeapon;

	UPROPERTY(ReplicatedUsing = OnRep_SecondaryWeapon)
	AWeapon* SecondaryWeapon;

	UPROPERTY(ReplicatedUsing=OnRep_Aiming)
	bool bAiming = false;
	
	UFUNCTION()
	void OnRep_Aiming();
	
	bool bAimButtonPressed = false;

	UPROPERTY(EditAnywhere)
	float BaseWalkSpeed;

	UPROPERTY(EditAnywhere)
	float AimWalkSpeed;

	UPROPERTY(EditAnywhere)
	float BaseCrosshairsSpread = 0.1f;

	bool bFireButtonPressed;

	float CrosshairsVelocityFactor;
	float CrosshairsInAirFactor;
	float CrosshairsAimFactor;
	float CrosshairsShootingFactor;

	UPROPERTY(EditAnywhere, Category="Combat")
	float AimingCrosshairsShrinkValue = 0.58f;

	UPROPERTY(EditAnywhere, Category="Combat")
	float ShootingCrosshairsSpreadValue = 0.1f;

	FVector HitTarget;

	float DefaultFOV;

	UPROPERTY(EditAnywhere, Category="Combat")
	float ZoomedFOV = 30.f;

	UPROPERTY(EditAnywhere, Category="Combat")
	float ZoomInterpSpeed = 20.f;

	void InterpFOV(float DeltaTime);
	void EquipPrimaryWeapon(AWeapon* WeaponToEquip);
	void EquipSecondaryWeapon(AWeapon* WeaponToEquip);

	float CurrentFOV;

	FHUDPackage HUDPackage;

	FTimerHandle FireTimer;

	bool bCanFire = true;

	void FireTimerStarted();

	UFUNCTION()
	void FireTimerFinished();

	bool CanFire() const;

	UPROPERTY(VisibleAnywhere, ReplicatedUsing=OnRep_CarriedAmmo)
	int32 CarriedAmmo;

	UFUNCTION()
	void OnRep_CarriedAmmo();

	//TMaps don't support replication
	UPROPERTY(EditAnywhere, Category="Ammo")
	TMap<EWeaponType, int32> CarriedAmmoMap;

	UPROPERTY(VisibleAnywhere, Category="Ammo", ReplicatedUsing=OnRep_CombatState)
	ECombatState CombatState = ECombatState::ECS_Unoccupied;

	UFUNCTION()
	void OnRep_CombatState();
	void UpdateAmmoValues();
	void UpdateShotgunAmmoValues();

	UPROPERTY(EditAnywhere, ReplicatedUsing=OnRep_Grenades)
	int32 Grenades = 0;

	UFUNCTION()
	void OnRep_Grenades();

	UPROPERTY(EditAnywhere)
	int32 MaxGrenades = 4;

	UPROPERTY(EditAnywhere)
	int32 MaxARCarriedAmmo = 90;

	UPROPERTY(EditAnywhere)
	int32 MaxPistolCarriedAmmo = 40;

	UPROPERTY(EditAnywhere)
	int32 MaxSMGCarriedAmmo = 120;

	UPROPERTY(EditAnywhere)
	int32 MaxRocketLauncherCarriedAmmo = 6;

	UPROPERTY(EditAnywhere)
	int32 MaxGrenadeLauncherCarriedAmmo = 8;

	UPROPERTY(EditAnywhere)
	int32 MaxShotgunCarriedAmmo = 20;

	UPROPERTY(EditAnywhere)
	int32 MaxCrossbowCarriedAmmo = 12;

	UPROPERTY(EditAnywhere)
	int32 MaxSniperRifleCarriedAmmo = 8;

	void UpdateHudGrenades();

	UPROPERTY(ReplicatedUsing=OnRep_HoldingTheFlag)
	bool bHoldingTheFlag = false;

	UFUNCTION()
	void OnRep_HoldingTheFlag();

	UPROPERTY()
	AWeapon* TheFlag;

public:
	FORCEINLINE int32 GetGrenades() const { return Grenades; }
	FORCEINLINE AWeapon* GetEquippedWeapon() const { return EquippedWeapon; }
	FORCEINLINE int32 GetCarriedAmmo() const { return CarriedAmmo; }
	bool ShouldSwapWeapons() const;
};
