// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayerStart.h"

#include "Components/SphereComponent.h"

AMyPlayerStart::AMyPlayerStart(const FObjectInitializer& ObjectInitializer) : APlayerStart(ObjectInitializer)
{
	SpawnCheckArea = CreateDefaultSubobject<USphereComponent>(TEXT("SpawnCheckArea"));
	SpawnCheckArea->SetupAttachment(RootComponent);
}
