// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerStart.h"
#include "MyPlayerStart.generated.h"

/**
 * 
 */
UCLASS()
class BLASTER_API AMyPlayerStart : public APlayerStart
{
	GENERATED_BODY()
public:
	AMyPlayerStart(const FObjectInitializer& ObjectInitializer);

private:
	UPROPERTY(VisibleAnywhere)
	class USphereComponent* SpawnCheckArea;
};
