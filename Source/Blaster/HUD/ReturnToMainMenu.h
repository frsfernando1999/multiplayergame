// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ReturnToMainMenu.generated.h"

/**
 * 
 */
UCLASS()
class BLASTER_API UReturnToMainMenu : public UUserWidget
{
	GENERATED_BODY()

public:
	void MenuSetup();
	void MenuTeardown();

protected:
	UFUNCTION()
	void OnDestroySession(bool bSuccessful);

	UFUNCTION()
	void OnPlayerLeftGame();

private:

	UPROPERTY()
	class APlayerController* PlayerController;

	UPROPERTY(meta=(BindWidget))
	class UButton* ExitButton;

	UFUNCTION()
	void ReturnButtonClicked();

	UPROPERTY()
	class UMultiplayerSessionsSubsystem* MultiplayerSessionsSubsystem;
	
public:
	
};
