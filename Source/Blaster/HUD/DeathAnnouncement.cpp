// Fill out your copyright notice in the Description page of Project Settings.


#include "DeathAnnouncement.h"

#include "Components/TextBlock.h"

void UDeathAnnouncement::SetDeathAnnouncementText(const FString& AttackerName, const FString& VictimName, bool bSuicide) const
{
	if (AnnouncementText)
	{
		const FString DeathAnnouncementText = GetRandomDeathMessage(AttackerName, VictimName, bSuicide);
		AnnouncementText->SetText(FText::FromString(DeathAnnouncementText));
	}
}

FString UDeathAnnouncement::GetRandomDeathMessage(const FString& AttackerName, const FString& VictimName, bool bSuicide) const
{
	if (bSuicide)
	{
		switch (FMath::RandRange(0, 2))
		{
		case 0:
			return FString::Printf(TEXT("%s ended your own life!"), *AttackerName);
		case 1:
			return FString::Printf(TEXT("%s shot yourself!"), *AttackerName);
		case 2:
			return FString::Printf(TEXT("%s commited suicide!"), *AttackerName);
		default: 
			return FString::Printf(TEXT("%s ended your own life!"), *AttackerName);
		}
	}
	if(AttackerName == VictimName && !bSuicide)
	{
		switch (FMath::RandRange(0, 2))
		{
		case 0:
			return FString::Printf(TEXT("%s ended his own life!"), *AttackerName);
		case 1:
			return FString::Printf(TEXT("%s shot himself!"), *AttackerName);
		case 2:
			return FString::Printf(TEXT("%s commited suicide!"), *AttackerName);
		default: 
			return FString::Printf(TEXT("%s ended his own life!"), *AttackerName);
		}
	}
	switch (FMath::RandRange(0, 4))
	{
	case 0:
		return FString::Printf(TEXT("%s killed %s!"), *AttackerName, *VictimName);
	case 1:
		return FString::Printf(TEXT("%s got sent to the Shadow Realm!"), *VictimName);
	case 2:
		return FString::Printf(TEXT("%s showed %s how it's done!"), *AttackerName, *VictimName);
	case 3:
		return FString::Printf(TEXT("%s can't get up!"), *VictimName);
	case 4:
		return FString::Printf(
			TEXT("Something about %s shooting %s, you get the picture."), *AttackerName, *VictimName);
	default:
		return FString::Printf(TEXT("%s killed %s!"), *AttackerName, *VictimName);
	}
}
