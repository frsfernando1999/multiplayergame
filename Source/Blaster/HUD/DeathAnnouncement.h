// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DeathAnnouncement.generated.h"

/**
 * 
 */
UCLASS()
class BLASTER_API UDeathAnnouncement : public UUserWidget
{
	GENERATED_BODY()

public:
	FString GetRandomDeathMessage(const FString& AttackerName, const FString& VictimName, bool bSuicide = false) const;
	void SetDeathAnnouncementText(const FString& AttackerName, const FString& VictimName, bool bSuicide = false) const;

	UPROPERTY(meta=(BindWidget))
	class UHorizontalBox* AnnouncementBox;

	UPROPERTY(meta=(BindWidget))
	class UTextBlock* AnnouncementText;

	
};
