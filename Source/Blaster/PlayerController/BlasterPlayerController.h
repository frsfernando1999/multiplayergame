// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blaster/Weapon/WeaponTypes.h"
#include "BlasterPlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FHighPingDelegate, bool, bPingTooHigh);

/**
 * 
 */
UCLASS()
class BLASTER_API ABlasterPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	virtual void Tick(float DeltaSeconds) override;
	void UpdateHUDPing() const;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void SetHudHealth(float Health, float MaxHealth);
	void SetHudShield(float Shield, float MaxShield);
	void SetHudScore(float Score);
	void SetHudDefeats(int32 Death);
	void SetHudWeaponAmmo(int32 Ammo);
	void SetHudCarriedAmmo(int32 Ammo);
	void SetHudCountdownTimer(float CountdownTime);
	void SetHudCarriedWeaponType(EWeaponType WeaponType);
	void SetHudAnnouncementCountdown(float CountdownTime);
	void SetHudGrenades(int32 Grenades);
	void HideTeamScores();
	void InitTeamScores();
	void SetHudRedTeamScore(int32 RedScore);
	void SetHudBlueTeamScore(int32 BlueScore);

	static FString GetWeaponTypeName(EWeaponType WeaponType);

	float GetServerTime() const; // Synced with server world clock


	virtual void OnPossess(APawn* InPawn) override;

	virtual void ReceivedPlayer() override;
	void CheckTimeSync(float DeltaSeconds);


	void OnMatchStateSet(FName State, bool bTeamsMatch = false);

	void HandleCooldown();

	bool bClientsInitialized = false;

	float SingleTripTime = 0.f;

	FHighPingDelegate HighPingDelegate;

	void BroadcastDeath(APlayerState* Attacker, APlayerState* Victim);

protected:
	virtual void BeginPlay() override;
	void SetHUDTime();
	void PollInit();

	virtual void SetupInputComponent() override;

	// Called on the client, executed on the server
	UFUNCTION(Server, Reliable)
	void ServerRequestServerTime(float TimeOfClientRequest);

	// Called on the server to send the time back to the client
	UFUNCTION(Client, Reliable)
	void ClientReportServerTime(float TimeOfClientRequest, float TimeServerReceivedClientRequest);

	//Difference between client and server time
	float ClientServerDelta = 0.f;

	UPROPERTY(EditAnywhere, Category="Time")
	float TimeSyncFrequency = 5.f;

	float TimeSyncRunningTime = 0.f;

	void ShowCharacterOverlay(bool bTeamsMatch = false);

	UFUNCTION(Server, Reliable)
	void ServerCheckMatchState();

	UFUNCTION(Client, Reliable)
	void ClientJoinedMidGame(FName StateOfMatch, float Warmup, float Match, float StartingTime, float CDTime);

	void HighPingWarning();
	void StopHighPingWarning();
	void CheckPing(float DeltaSeconds);

	void ShowReturnToMainMenu();

	UFUNCTION(Client, Reliable)
	void ClientDeathAnnouncement(APlayerState* Attacker, APlayerState* Victim);

	UPROPERTY(ReplicatedUsing=OnRep_ShowTeamsScore)
	bool bShowTeamScores = false;

	UFUNCTION()
	void OnRep_ShowTeamsScore();

	FString GetInfoText(const TArray<class ABlasterPlayerState*>& Players) const;
	static FString GetTeamsInfoText(const class ABlasterGameState* BlasterGameState);

private:
	UPROPERTY(EditAnywhere, Category="HUD")
	TSubclassOf<class UUserWidget> ReturnToMainMenuWidget;

	UPROPERTY()
	class UReturnToMainMenu* ReturnToMainMenu;

	bool bReturnToMainMenuOpen = false;

	UPROPERTY()
	class ABlasterHUD* BlasterHUD;

	UPROPERTY()
	class ABlasterGameMode* BlasterGameMode;

	float LevelStartingTime = 0.f;
	float MatchTime = 0.f;
	float WarmupTime = 0.f;
	float CooldownTime = 0.f;

	int32 CountdownInt = 0;

	UPROPERTY(ReplicatedUsing=OnRep_MatchState)
	FName MatchState;

	UFUNCTION()
	void OnRep_MatchState();

	UPROPERTY()
	class UCharacterOverlay* CharacterOverlay;

	float HudHealth;
	bool bInitializeHealth = false;
	float HudMaxHealth;

	float HudShield;
	bool bInitializeShield = false;
	float HudMaxShield;

	float HudScore;
	bool bInitializeScore = false;

	int32 HudWeaponAmmo;
	bool bInitializeWeaponAmmo = false;

	int32 HudCarriedAmmo;
	bool bInitializeCarriedAmmo = false;

	EWeaponType HudWeaponType;
	bool bInitializeWeaponType = false;

	int32 HudDefeats;
	bool bInitializeDefeats = false;

	int32 HudGrenades;
	bool bInitializeGrenades = false;

	float HighPingRunningTime = 0.f;

	UPROPERTY(EditAnywhere)
	float HighPingAnimationDuration = 2.5f;

	UPROPERTY(EditAnywhere)
	float PingAnimationRunningTime = 0.f;

	UPROPERTY(EditAnywhere)
	float CheckPingFrequency = 5.f;

	UFUNCTION(Server, Reliable)
	void ServerReportPingStatus(bool bHighPing);

	UPROPERTY(EditAnywhere)
	float HighPingThreshold = 70.f;

	

public:
	bool GetIsTeamsGameMode() const { return bShowTeamScores; }
};
