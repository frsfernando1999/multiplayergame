// Fill out your copyright notice in the Description page of Project Settings.


#include "HitscanWeapon.h"

#include "Blaster/Blaster.h"
#include "Blaster/BlasterComponents/LagCompensationComponent.h"
#include "Blaster/Character/BlasterCharacter.h"
#include "Blaster/PlayerController/BlasterPlayerController.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Sound/SoundCue.h"

void AHitscanWeapon::Fire(const FVector& HitTarget)
{
	Super::Fire(HitTarget);

	APawn* OwnerPawn = Cast<APawn>(GetOwner());
	if (OwnerPawn == nullptr) return;
	if (const USkeletalMeshSocket* MuzzleFlashSocket = GetWeaponMesh()->GetSocketByName("MuzzleFlash"))
	{
		FTransform SocketTransform = MuzzleFlashSocket->GetSocketTransform(GetWeaponMesh());
		FVector Start = SocketTransform.GetLocation();

		FHitResult FireHit;
		WeaponTraceHit(Start, HitTarget, FireHit);

		if (FireHit.bBlockingHit)
		{
			AController* InstigatorController = OwnerPawn->GetController();
			if (ABlasterCharacter* BlasterCharacter = Cast<ABlasterCharacter>(FireHit.GetActor()))
			{
				//Instigator controller is null on simulated proxies
				if (InstigatorController)
				{
					bool bCauseAuthDamage =  !bUseServerSideRewind || OwnerPawn->IsLocallyControlled();
					if (HasAuthority() && bCauseAuthDamage)
					{
						const float DamageToCause = FireHit.BoneName.ToString() == FString("head") ? HeadshotDamage : Damage;

						UGameplayStatics::ApplyDamage(
							BlasterCharacter,
							DamageToCause,
							InstigatorController,
							this,
							UDamageType::StaticClass()
						);
					}
					if (!HasAuthority() && bUseServerSideRewind)
					{
						BlasterOwnerCharacter = BlasterOwnerCharacter == nullptr
							                        ? Cast<ABlasterCharacter>(OwnerPawn)
							                        : BlasterOwnerCharacter;
						BlasterOwnerController = BlasterOwnerController == nullptr
							                         ? Cast<ABlasterPlayerController>(InstigatorController)
							                         : BlasterOwnerController;
						if (BlasterOwnerController && BlasterOwnerCharacter && BlasterOwnerCharacter->
							GetLagCompensation() && BlasterOwnerCharacter->IsLocallyControlled())
						{
							BlasterOwnerCharacter->GetLagCompensation()->ServerScoreRequestHitscan(BlasterCharacter,
								Start, HitTarget,
								BlasterOwnerController->GetServerTime() - BlasterOwnerController->SingleTripTime);
						}
					}
				}
			}
			DetermineSurfaceToPlaySoundAndParticles(FireHit);
		}
		PlayFireSoundAndParticles(SocketTransform);
	}
}

void AHitscanWeapon::WeaponTraceHit(const FVector& TraceStart, const FVector& HitTarget, FHitResult& OutHit) const
{
	if (const UWorld* World = GetWorld())
	{
		const FVector End = TraceStart + (HitTarget - TraceStart) * 1.25;
		FCollisionQueryParams CollisionQueryParams;
		CollisionQueryParams.bReturnPhysicalMaterial = true;
		World->LineTraceSingleByChannel(OutHit, TraceStart, End, ECC_Visibility, CollisionQueryParams);
		FVector BeamEnd = End;
		if (OutHit.bBlockingHit)
		{
			BeamEnd = OutHit.ImpactPoint;
		}
		else
		{
			OutHit.ImpactPoint = End;
		}
		SpawnBeamParticles(TraceStart, BeamEnd);
	}
}

void AHitscanWeapon::DetermineSurfaceToPlaySoundAndParticles(const FHitResult& FireHit)
{
	switch (UPhysicalMaterial::DetermineSurfaceType(FireHit.PhysMaterial.Get()))
	{
	case EPS_Grass:
		PlaySoundAndParticles(FireHit, ImpactRockSound, ImpactRockParticles);
		break;
	case EPS_Stone:
		PlaySoundAndParticles(FireHit, ImpactRockSound, ImpactRockParticles);
		break;
	case EPS_Tile:
		PlaySoundAndParticles(FireHit, ImpactWorldSound, ImpactWorldParticles);
		break;
	case EPS_Metal:
		PlaySoundAndParticles(FireHit, ImpactMetalSound, ImpactMetalParticles);
		break;
	case EPS_Wood:
		PlaySoundAndParticles(FireHit, ImpactWoodSound, ImpactWoodParticles);
		break;
	case EPS_Flesh:
		PlaySoundAndParticles(FireHit, ImpactFleshSound, ImpactFleshParticles);
		break;
	default:
		PlaySoundAndParticles(FireHit, ImpactWorldSound, ImpactWorldParticles);
		break;
	}
}

void AHitscanWeapon::PlaySoundAndParticles(const FHitResult& FireHit, USoundCue* Sound, UParticleSystem* Particles,
                                           float VolumeMultiplier, float PitchMultiplier) const
{
	if (Sound && Particles)
	{
		UGameplayStatics::PlaySoundAtLocation(this, Sound, FireHit.ImpactPoint, FRotator::ZeroRotator, VolumeMultiplier,
		                                      PitchMultiplier);
		UGameplayStatics::SpawnEmitterAtLocation(this, Particles, FireHit.ImpactPoint);
	}
}

void AHitscanWeapon::SpawnBeamParticles(const FVector& StartLocation, const FVector& BeamEnd) const
{
	if (BeamParticles)
	{
		if (UParticleSystemComponent* Beam = UGameplayStatics::SpawnEmitterAtLocation(
			GetWorld(), BeamParticles, StartLocation, FRotator::ZeroRotator, true))
		{
			Beam->SetVectorParameter(FName("Target"), BeamEnd);
		}
	}
}

void AHitscanWeapon::PlayFireSoundAndParticles(const FTransform& SocketTransform) const
{
	if (MuzzleFlash)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), MuzzleFlash, SocketTransform);
	}
	if (FireSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}
}
