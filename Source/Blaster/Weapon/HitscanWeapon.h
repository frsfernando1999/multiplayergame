// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "HitscanWeapon.generated.h"

/**
 * 
 */
UCLASS()
class BLASTER_API AHitscanWeapon : public AWeapon
{
	GENERATED_BODY()
public:
	virtual void Fire(const FVector& HitTarget) override;

protected:
	void SpawnBeamParticles(const FVector& StartLocation, const FVector& BeamEnd) const;
	void PlayFireSoundAndParticles(const FTransform& SocketTransform) const;
	void WeaponTraceHit(const FVector& TraceStart, const FVector& HitTarget, FHitResult& OutHit) const;
	void DetermineSurfaceToPlaySoundAndParticles(const FHitResult& FireHit);
	void PlaySoundAndParticles(const FHitResult& FireHit, USoundCue* Sound, UParticleSystem* Particles,
	                           float VolumeMultiplier = 0.7f, float PitchMultiplier = 1.f) const;
	
	UPROPERTY(EditAnywhere)
	UParticleSystem* ImpactWorldParticles;

	UPROPERTY(EditAnywhere)
	class USoundCue* ImpactWorldSound;

	UPROPERTY(EditAnywhere)
	UParticleSystem* ImpactFleshParticles;

	UPROPERTY(EditAnywhere)
	USoundCue* ImpactFleshSound;

	UPROPERTY(EditAnywhere)
	UParticleSystem* ImpactWoodParticles;

	UPROPERTY(EditAnywhere)
	class USoundCue* ImpactWoodSound;

	UPROPERTY(EditAnywhere)
	UParticleSystem* ImpactRockParticles;

	UPROPERTY(EditAnywhere)
	USoundCue* ImpactRockSound;

	UPROPERTY(EditAnywhere)
	UParticleSystem* ImpactMetalParticles;

	UPROPERTY(EditAnywhere)
	USoundCue* ImpactMetalSound;

	UPROPERTY(EditAnywhere)
	UParticleSystem* BeamParticles;

	UPROPERTY(EditAnywhere)
	UParticleSystem* MuzzleFlash;

	UPROPERTY(EditAnywhere)
	USoundCue* FireSound;

public:
};
