// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"

#include "NiagaraFunctionLibrary.h"
#include "Blaster/Blaster.h"
#include "Blaster/Character/BlasterCharacter.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Sound/SoundCue.h"

AProjectile::AProjectile()
{
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;

	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	SetRootComponent(CollisionBox);
	CollisionBox->SetCollisionObjectType(ECC_WorldDynamic);
	CollisionBox->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	CollisionBox->SetCollisionResponseToChannels(ECR_Ignore);
	CollisionBox->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	CollisionBox->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	CollisionBox->SetCollisionResponseToChannel(ECC_SkeletalMesh, ECR_Block);
	CollisionBox->bReturnMaterialOnMove = true;
}

void AProjectile::BeginPlay()
{
	Super::BeginPlay();

	if (Tracer)
	{
		TracerComponent = UGameplayStatics::SpawnEmitterAttached(
			Tracer,
			CollisionBox,
			FName(),
			GetActorLocation(),
			GetActorRotation(),
			EAttachLocation::KeepWorldPosition
		);
	}

	if (HasAuthority())
	{
		CollisionBox->OnComponentHit.AddDynamic(this, &AProjectile::OnHit);
	}
}

void AProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
                        FVector NormalImpulse, const FHitResult& Hit)
{
	if (HasAuthority())
	{
		Multicast_OnHit(Hit);
	}
	Destroy();
}

void AProjectile::Multicast_OnHit_Implementation(const FHitResult& Hit)
{
	DetermineSurfaceToPlaySoundAndParticles(Hit);
}

void AProjectile::DetermineSurfaceToPlaySoundAndParticles(const FHitResult& FireHit)
{
	switch (UPhysicalMaterial::DetermineSurfaceType(FireHit.PhysMaterial.Get()))
	{
	case EPS_Grass:
		PlaySoundAndParticles(FireHit, ImpactRockSound, ImpactRockParticles);
		break;
	case EPS_Stone:
		PlaySoundAndParticles(FireHit, ImpactRockSound, ImpactRockParticles);
		break;
	case EPS_Tile:
		PlaySoundAndParticles(FireHit, ImpactWorldSound, ImpactWorldParticles);
		break;
	case EPS_Metal:
		PlaySoundAndParticles(FireHit, ImpactMetalSound, ImpactMetalParticles);
		break;
	case EPS_Wood:
		PlaySoundAndParticles(FireHit, ImpactWoodSound, ImpactWoodParticles);
		break;
	case EPS_Flesh:
		PlaySoundAndParticles(FireHit, ImpactFleshSound, ImpactFleshParticles);
		break;
	default:
		PlaySoundAndParticles(FireHit, ImpactWorldSound, ImpactWorldParticles);
		break;
	}
}

void AProjectile::PlaySoundAndParticles(const FHitResult& FireHit, USoundCue* Sound, UParticleSystem* Particles, float VolumeMultiplier, float PitchMultiplier) const
{
	if (Sound && Particles)
	{
		UGameplayStatics::PlaySoundAtLocation(this, Sound, FireHit.ImpactPoint, FRotator::ZeroRotator, VolumeMultiplier, PitchMultiplier);
		UGameplayStatics::SpawnEmitterAtLocation(this, Particles, FireHit.ImpactPoint);
	}
}

void AProjectile::Destroyed()
{
	Super::Destroyed();
	if (!bShouldPlayDestroyParticles) return;		
	
	if (ImpactWorldParticles)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactWorldParticles, GetActorTransform());
	}
	if (ImpactWorldSound)
	{
		UGameplayStatics::SpawnSoundAtLocation(this, ImpactWorldSound, GetActorLocation());
	}
}

void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AProjectile::SpawnTrailSystem()
{
	if (TrailSystem)
	{
		TrailSystemComponent = UNiagaraFunctionLibrary::SpawnSystemAttached(TrailSystem, GetRootComponent(), FName(), GetActorLocation(),
																			GetActorRotation(), EAttachLocation::KeepWorldPosition, false);
	}
}


void AProjectile::StartDestroyTimer()
{
	GetWorldTimerManager().SetTimer(DestroyTimer, this, &AProjectile::DestroyTimerFinished, DestroyTime);
}

void AProjectile::DestroyTimerFinished()
{
	Destroy();
}

void AProjectile::ExplosionDamage()
{
	if (const APawn* FiringPawn = GetInstigator())
	{
		AController* FiringController = FiringPawn->GetController();
		if (FiringController && HasAuthority())
		{
			UGameplayStatics::ApplyRadialDamageWithFalloff(
				this,
				Damage,
				Damage * 0.3,
				GetActorLocation(),
				DamageInnerRadius,
				DamageOuterRadius,
				DamageFalloff,
				UDamageType::StaticClass(),
				TArray<AActor*>(),
				this,
				FiringController
			);
		}
	}
}