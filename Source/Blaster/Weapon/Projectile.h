// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

UCLASS()
class BLASTER_API AProjectile : public AActor
{
	GENERATED_BODY()

public:
	AProjectile();
	virtual void Tick(float DeltaTime) override;

	bool bUseServerSideRewind = false;

	FVector_NetQuantize TraceStart;
	FVector_NetQuantize100 InitialVelocity;

	UPROPERTY(EditAnywhere)
	float InitialSpeed = 30000.f;

	// Only set this for grenades and rockets	
	UPROPERTY(EditAnywhere)
	float Damage = 20.f;

	// Doesn't matter for grenades and rockets
	UPROPERTY(EditAnywhere)
	float HeadshotDamage = 40.f;

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void OnHit(
		UPrimitiveComponent* HitComp,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		FVector NormalImpulse,
		const FHitResult& Hit);

	UFUNCTION(NetMulticast, Reliable)
	void Multicast_OnHit(const FHitResult& Hit);
	void SpawnTrailSystem();
	void DetermineSurfaceToPlaySoundAndParticles(const FHitResult& FireHit);
	void PlaySoundAndParticles(const FHitResult& FireHit, class USoundCue* Sound, UParticleSystem* Particles,
	                           float VolumeMultiplier = 0.7f, float PitchMultiplier = 1.f) const;

	void ExplosionDamage();
	void StartDestroyTimer(); 
	void DestroyTimerFinished();

	virtual void Destroyed() override;

	UPROPERTY()
	class UNiagaraComponent* TrailSystemComponent;

	UPROPERTY(EditAnywhere)
	class UNiagaraSystem* TrailSystem;

	UPROPERTY(EditAnywhere)
	UParticleSystem* ImpactWorldParticles;

	UPROPERTY(EditAnywhere)
	USoundCue* ImpactWorldSound;

	UPROPERTY(EditAnywhere)
	UParticleSystem* ImpactFleshParticles;

	UPROPERTY(EditAnywhere)
	USoundCue* ImpactFleshSound;

	UPROPERTY(EditAnywhere)
	UParticleSystem* ImpactWoodParticles;

	UPROPERTY(EditAnywhere)
	class USoundCue* ImpactWoodSound;

	UPROPERTY(EditAnywhere)
	UParticleSystem* ImpactRockParticles;

	UPROPERTY(EditAnywhere)
	USoundCue* ImpactRockSound;

	UPROPERTY(EditAnywhere)
	UParticleSystem* ImpactMetalParticles;

	UPROPERTY(EditAnywhere)
	USoundCue* ImpactMetalSound;

	UPROPERTY(EditAnywhere)
	class UBoxComponent* CollisionBox;

	UPROPERTY(VisibleAnywhere)
	class UProjectileMovementComponent* ProjectileMovementComponent;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* ProjectileMesh;

	UPROPERTY(EditAnywhere)
	float DamageInnerRadius = 200.f;
	
	UPROPERTY(EditAnywhere)
	float DamageOuterRadius = 500.f;

	UPROPERTY(EditAnywhere)
	float DamageFalloff = 1.f;
	
	UPROPERTY(EditAnywhere)
	bool bShouldPlayDestroyParticles = false;
	
private:
	FTimerHandle DestroyTimer;	

	UPROPERTY(EditAnywhere)
	float DestroyTime = 3.f;

	UPROPERTY(EditAnywhere)
	UParticleSystem* Tracer;

	UPROPERTY()
	UParticleSystemComponent* TracerComponent;


public:
};
