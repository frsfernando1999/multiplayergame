﻿#pragma once

#define TRACE_LENGTH 50'000.F

#define CUSTOM_DEPTH_WHITE 250
#define CUSTOM_DEPTH_GREEN 251
#define CUSTOM_DEPTH_BLUE 252
#define CUSTOM_DEPTH_PURPLE 253
#define CUSTOM_DEPTH_ORANGE 254

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	EWT_AssaultRifle UMETA(DisplayName="Assault Rifle"),
	EWT_RocketLauncher UMETA(DisplayName="Rocket Launcher"),
	EWT_Pistol UMETA(DisplayName="Pistol"),
	EWT_Crossbow UMETA(DisplayName="Crossbow"),
	EWT_SMG UMETA(DisplayName="SMG"),
	EWT_Shotgun UMETA(DisplayName="Shotgun"),
	EWT_SniperRifle UMETA(DisplayName="Sniper Rifle"),
	EWT_GrenadeLauncher UMETA(DisplayName="Grenade Launcher"),
	EWT_Flag UMETA(DisplayName="Flag"),
	
	EWT_MAX UMETA(DisplayName="Default MAX")
};