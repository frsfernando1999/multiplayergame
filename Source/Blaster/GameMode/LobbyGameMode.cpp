// Fill out your copyright notice in the Description page of Project Settings.


#include "LobbyGameMode.h"

#include "MultiplayerSessionsSubsystem.h"
#include "GameFramework/GameStateBase.h"
#include "Blaster/HUD/OverheadWidget.h"

void ALobbyGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	const int32 NumberOfPlayers = GameState.Get()->PlayerArray.Num();

	UGameInstance* GameInstance = GetGameInstance();
	if (GameInstance)
	{
		UMultiplayerSessionsSubsystem* Subsystem = GameInstance->GetSubsystem<UMultiplayerSessionsSubsystem>();
		check(Subsystem);

		const FInputModeGameOnly InputMode;

		EnableInput(NewPlayer);

		if (NumberOfPlayers == Subsystem->DesiredNumPublicConnections)
		{
			if (UWorld* World = GetWorld())
			{
				FString MatchType = Subsystem->DesiredMatchType;
				if (MatchType == "Teams")
				{
					World->ServerTravel(FString("/Game/Maps/Teams?listen"));
				}
				else if (MatchType == "CaptureTheFlag")
				{
					World->ServerTravel(FString("/Game/Maps/CaptureTheFlag?listen"));
				}
				else 
				{
					World->ServerTravel(FString("/Game/Maps/MainGame?listen"));
				}
			}
		}
	}
}
