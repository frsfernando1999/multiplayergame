// Fill out your copyright notice in the Description page of Project Settings.


#include "BlasterGameMode.h"

#include "Blaster/Character/BlasterCharacter.h"
#include "Blaster/GameState/BlasterGameState.h"
#include "Blaster/PlayerController/BlasterPlayerController.h"
#include "Blaster/PlayerStarts/MyPlayerStart.h"
#include "Blaster/PlayerStarts/TeamPlayerStart.h"
#include "Blaster/PlayerStates/BlasterPlayerState.h"
#include "Kismet/GameplayStatics.h"

namespace MatchState
{
	const FName Cooldown = FName("Cooldown");
}

ABlasterGameMode::ABlasterGameMode()
{
	bDelayedStart = true;
}

void ABlasterGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (MatchState == MatchState::WaitingToStart)
	{
		CountdownTime = WarmupTime - GetWorld()->GetTimeSeconds() + LevelStartingTime;
		if (CountdownTime <= 0.f)
		{
			StartMatch();
		}
	}
	else if (MatchState == MatchState::InProgress)
	{
		CountdownTime = WarmupTime + MatchTime - GetWorld()->GetTimeSeconds() + LevelStartingTime;
		if (CountdownTime <= 0.f)
		{
			SetMatchState(MatchState::Cooldown);
		}
	}
	else if (MatchState == MatchState::Cooldown)
	{
		CountdownTime = CooldownTime + WarmupTime + MatchTime - GetWorld()->GetTimeSeconds() + LevelStartingTime;
		if (CountdownTime <= 0.f)
		{
			RestartGame();
		}
	}
}

void ABlasterGameMode::BeginPlay()
{
	Super::BeginPlay();
	LevelStartingTime = GetWorld()->GetTimeSeconds();
}

void ABlasterGameMode::OnMatchStateSet()
{
	Super::OnMatchStateSet();

	// Function for looping through all player controllers in the world.

	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		//Can be accessed after being dereferenced (*It)
		if (ABlasterPlayerController* BlasterPlayer = Cast<ABlasterPlayerController>(*It))
		{
			BlasterPlayer->OnMatchStateSet(MatchState, bTeamsMatch);
		}
	}
}


void ABlasterGameMode::PlayerEliminated(ABlasterCharacter* EliminatedCharacter,
                                        ABlasterPlayerController* VictimController,
                                        ABlasterPlayerController* AttackerController)
{
	ABlasterPlayerState* AttackerPlayerState = AttackerController
		                                           ? Cast<ABlasterPlayerState>(AttackerController->PlayerState)
		                                           : nullptr;
	ABlasterPlayerState* VictimPlayerState = VictimController
		                                         ? Cast<ABlasterPlayerState>(VictimController->PlayerState)
		                                         : nullptr;

	ABlasterGameState* BlasterGameState = GetGameState<ABlasterGameState>();

	if (AttackerPlayerState && AttackerPlayerState != VictimPlayerState && BlasterGameState)
	{
		TArray<ABlasterPlayerState*> PlayersCurrentlyInTheLead;
		for (auto LeadPlayer : BlasterGameState->TopScoringPlayers)
		{
			PlayersCurrentlyInTheLead.Add(LeadPlayer);
		}

		AttackerPlayerState->AddToScore(1.0f);
		BlasterGameState->UpdateTopScore(AttackerPlayerState);
		if (BlasterGameState->TopScoringPlayers.Contains(AttackerPlayerState))
		{
			if (ABlasterCharacter* Leader = Cast<ABlasterCharacter>(AttackerPlayerState->GetPawn()))
			{
				Leader->MulticastGainTheLead();
			}
		}

		for (int32 i = 0; i < PlayersCurrentlyInTheLead.Num(); i++)
		{
			if (!BlasterGameState->TopScoringPlayers.Contains(PlayersCurrentlyInTheLead[i]))
			{
				if (ABlasterCharacter* Loser = Cast<ABlasterCharacter>(PlayersCurrentlyInTheLead[i]->GetPawn()))
				{
					Loser->MulticastLostTheLead();
				}
			}
		}
	}

	if (VictimPlayerState)
	{
		VictimPlayerState->AddToDeaths(1);
	}

	if (EliminatedCharacter)
	{
		EliminatedCharacter->Elim(false);
	}

	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		//Use dereference operator
		ABlasterPlayerController* BlasterPlayer = Cast<ABlasterPlayerController>(*It);
		if (BlasterPlayer && AttackerPlayerState && VictimPlayerState)
		{
			BlasterPlayer->BroadcastDeath(AttackerPlayerState, VictimPlayerState);
		}
	}
}

void ABlasterGameMode::RequestRespawn(ACharacter* DeadCharacter, AController* EliminatedController)
{
	if (DeadCharacter)
	{
		DeadCharacter->Reset();
		DeadCharacter->Destroy();
	}
	if (EliminatedController)
	{
		TArray<AActor*> PlayerStarts;
		UGameplayStatics::GetAllActorsOfClass(this, AMyPlayerStart::StaticClass(), PlayerStarts);
		if (PlayerStarts.Num() > 0)
		{
			TArray<AActor*> ValidPlayerRespawnArea;
			for (AActor* PlayerStart : PlayerStarts)
			{
				TArray<AActor*> Characters;
				PlayerStart->GetOverlappingActors(Characters, ACharacter::StaticClass());
				if (Characters.Num() == 0)
				{
					ValidPlayerRespawnArea.Add(PlayerStart);
				}
			}

			if (ValidPlayerRespawnArea.Num() == 0)
			{
				const int32 RandomIndex = FMath::RandRange(0, PlayerStarts.Num() - 1);
				RestartPlayerAtPlayerStart(EliminatedController, PlayerStarts[RandomIndex]);
			}
			else
			{
				const int32 RandomIndex = FMath::RandRange(0, ValidPlayerRespawnArea.Num() - 1);
				RestartPlayerAtPlayerStart(EliminatedController, ValidPlayerRespawnArea[RandomIndex]);
			}
		}
		else
		{
			UGameplayStatics::GetAllActorsOfClass(this, ATeamPlayerStart::StaticClass(), PlayerStarts);
			TArray<ATeamPlayerStart*> TeamPlayerStarts;
			ABlasterPlayerState* PlayerState = Cast<ABlasterPlayerState>(EliminatedController->PlayerState);
			if (PlayerState)
			{
				for (auto Start : PlayerStarts)
				{
					ATeamPlayerStart* TeamStart = Cast<ATeamPlayerStart>(Start);
					if (TeamStart && TeamStart->Team == PlayerState->GetTeam())
					{
						TeamPlayerStarts.Add(TeamStart);
					}
				}
				if (TeamPlayerStarts.Num() > 0)
				{
					ATeamPlayerStart* ChosenPlayerStart = TeamPlayerStarts[FMath::RandRange(
						0, TeamPlayerStarts.Num() - 1)];
					RestartPlayerAtPlayerStart(EliminatedController, ChosenPlayerStart);
				}
			}
			else
			{
				RestartPlayerAtPlayerStart(EliminatedController, PlayerStarts[FMath::RandRange(
					0, TeamPlayerStarts.Num() - 1)]);
				
			}
		}
	}
}

void ABlasterGameMode::PlayerLeftGame(ABlasterPlayerState* PlayerLeaving) const
{
	if (PlayerLeaving == nullptr) return;

	ABlasterGameState* BlasterGameState = GetGameState<ABlasterGameState>();

	if (BlasterGameState && BlasterGameState->TopScoringPlayers.Contains(PlayerLeaving))
	{
		BlasterGameState->TopScoringPlayers.Remove(PlayerLeaving);
	}

	if (ABlasterCharacter* CharacterLeaving = Cast<ABlasterCharacter>(PlayerLeaving->GetPawn()))
	{
		CharacterLeaving->Elim(true);
	}
}

float ABlasterGameMode::CalculateDamage(AController* Attacker, AController* Victim, float BaseDamage)
{
	return BaseDamage;
}
