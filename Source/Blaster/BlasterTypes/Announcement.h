﻿#pragma once

namespace Announcement
{
	const FString NewMatchStartsIn("New Match Starts In:");
	const FString ThereIsNoWinner("There is no Winner.");
	const FString YouWon("Congratulations! You Won!");
	const FString PlayersTiedForTheWin("Players tied for the win:\n");
	const FString TeamTie("Draw!");
	const FString RedTeamWins("Red Team Wins!");
	const FString BlueTeamWins("Blue Team Wins");
}
