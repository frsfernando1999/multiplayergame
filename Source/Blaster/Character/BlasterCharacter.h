// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blaster/BlasterComponents/CombatComponent.h"
#include "Blaster/BlasterTypes/CombatState.h"
#include "Blaster/BlasterTypes/Team.h"
#include "Blaster/BlasterTypes/TurningInPlace.h"
#include "Blaster/Interfaces/InteractWithCrosshairsInterface.h"
#include "Components/TimelineComponent.h"
#include "GameFramework/Character.h"
#include "BlasterCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnLeftGame);

UCLASS()
class BLASTER_API ABlasterCharacter : public ACharacter, public IInteractWithCrosshairsInterface
{
	GENERATED_BODY()

public:
	ABlasterCharacter();
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	void UpdateHudHealth();
	void UpdateHudShield();
	void UpdateHudWeaponStats();
	virtual void PostInitializeComponents() override;
	virtual void OnRep_ReplicatedMovement() override;

	void PlayFireMontage(bool bAiming) const;
	void PlayReloadMontage() const;
	void PlayDeathMontage() const;
	void PlayThrowGrenadeMontage() const;
	void PlaySwapWeaponMontage() const;

	void Elim(bool bPlayerLeftGame);

	UFUNCTION(NetMulticast, Reliable)
	void Multicast_Elim(bool bPlayerLeftGame);

	UFUNCTION(BlueprintImplementableEvent)
	void ShowSniperScopeWidget(bool bShowScope);

	virtual void Destroyed() override;

	UPROPERTY(Replicated)
	bool bDisableGameplay = false;

	void SpawnDefaultWeapon();

	UPROPERTY()
	TMap<FName, class UBoxComponent*> HitCollisionBoxes;

	bool bFinishedSwapping = false;

	UFUNCTION(Server, Reliable)
	void ServerLeaveGame();

	FOnLeftGame OnLeftGame;

	UFUNCTION(NetMulticast, Reliable)
	void MulticastGainTheLead();
	
	UFUNCTION(NetMulticast, Reliable)
	void MulticastLostTheLead();

	void SetTeamColor(ETeam Team);

	void SetSpawnPoint();

protected:
	virtual void BeginPlay() override;
	void RotateInPlace(float DeltaTime);

	void MoveForward(float Value);
	void MoveRight(float Value);
	void Turn(float Value);
	void LookUp(float Value);
	void EquipButtonPressed();
	void CrouchButtonPressed();
	void ReloadButtonPressed();
	void FireButtonPressed();
	void FireButtonReleased();
	void AimButtonPressed();
	void AimButtonReleased();
	void GrenadeButtonPressed();
	float CalculateSpeed() const;
	void CalculateAO_Pitch();
	void AimOffset(float DeltaTime);
	void SimProxiesTurn();
	virtual void Jump() override;
	void PlayHitReactMontage() const;
	void PollInit();
	static void DropOrDestroyWeapon(AWeapon* Weapon);

	UFUNCTION(BlueprintCallable)
	EPhysicalSurface GetSurfaceType();

	UFUNCTION()
	void ReceiveDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	                   AController* InstigatorController, AActor* DamageCauser);


	UPROPERTY(EditAnywhere)
	UBoxComponent* head;

	UPROPERTY(EditAnywhere)
	UBoxComponent* Pelvis;

	UPROPERTY(EditAnywhere)
	UBoxComponent* spine_02;

	UPROPERTY(EditAnywhere)
	UBoxComponent* spine_03;

	UPROPERTY(EditAnywhere)
	UBoxComponent* upperarm_l;

	UPROPERTY(EditAnywhere)
	UBoxComponent* upperarm_r;

	UPROPERTY(EditAnywhere)
	UBoxComponent* lowerarm_l;

	UPROPERTY(EditAnywhere)
	UBoxComponent* lowerarm_r;

	UPROPERTY(EditAnywhere)
	UBoxComponent* hand_l;

	UPROPERTY(EditAnywhere)
	UBoxComponent* right_hand;

	UPROPERTY(EditAnywhere)
	UBoxComponent* backpack;

	UPROPERTY(EditAnywhere)
	UBoxComponent* blanket_l;

	UPROPERTY(EditAnywhere)
	UBoxComponent* thigh_l;

	UPROPERTY(EditAnywhere)
	UBoxComponent* thigh_r;

	UPROPERTY(EditAnywhere)
	UBoxComponent* calf_l;

	UPROPERTY(EditAnywhere)
	UBoxComponent* calf_r;

	UPROPERTY(EditAnywhere)
	UBoxComponent* foot_l;

	UPROPERTY(EditAnywhere)
	UBoxComponent* foot_r;

private:
	UPROPERTY(VisibleAnywhere, Category="Visibility")
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, Category="Visibility")
	class UCameraComponent* FollowCamera;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"))
	class UWidgetComponent* OverheadWidget;

	UPROPERTY(ReplicatedUsing = OnRep_OverlappingWeapon)
	class AWeapon* OverlappingWeapon;

	UFUNCTION()
	void OnRep_OverlappingWeapon(AWeapon* LastWeapon) const;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"))
	class UCombatComponent* CombatComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"))
	class UBuffComponent* Buff;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"))
	class ULagCompensationComponent* LagCompensation;

	UPROPERTY(EditAnywhere, Category="Visibility")
	float CameraThreshold = 200.f;

	// Unreal engine will generate files for this, we should only define an implementation
	UFUNCTION(Server, Reliable)
	void ServerEquipButtonPressed();

	float AO_Yaw;
	float InterpAO_Yaw;
	float AO_Pitch;
	FRotator StartingAimRotation;

	ETurningInPlace TurningInPlace;

	void TurnInPlace(float DeltaTime);

	UPROPERTY(EditAnywhere, Category="Montages")
	UAnimMontage* FireWeaponMontage;

	UPROPERTY(EditAnywhere, Category="Montages")
	UAnimMontage* DeathWeaponMontage;

	UPROPERTY(EditAnywhere, Category="Montages")
	UAnimMontage* HitReactMontage;

	UPROPERTY(EditAnywhere, Category="Montages")
	UAnimMontage* ReloadMontage;

	UPROPERTY(EditAnywhere, Category="Montages")
	UAnimMontage* ThrowGrenadeMontage;

	UPROPERTY(EditAnywhere, Category="Montages")
	UAnimMontage* SwapWeapon;

	void HideCharacterIfCameraClose() const;

	bool bRotateRootBone;
	float TurnThreshold = 0.5f;
	FRotator ProxyRotationLastFrame;
	FRotator ProxyRotation;
	float ProxyYaw;
	float TimeSinceLastMovementReplication;

	UPROPERTY(EditAnywhere, Category="Player Stats")
	float MaxHealth = 100;

	UPROPERTY(VisibleAnywhere, ReplicatedUsing=OnRep_Health, Category="Player Stats")
	float Health = 100.f;

	UFUNCTION()
	void OnRep_Health(float LastHealth);

	UPROPERTY(EditAnywhere, Category="Player Stats")
	float MaxShield = 100;

	UPROPERTY(EditAnywhere, ReplicatedUsing=OnRep_Shield, Category="Player Stats")
	float Shield = 50.f;

	UFUNCTION()
	void OnRep_Shield(float LastShield);

	bool bIsDead = false;

	UPROPERTY()
	class ABlasterPlayerController* BlasterPlayerController;

	FTimerHandle DeathTimer;

	UPROPERTY(EditDefaultsOnly, Category="Death")
	float DeathDelay = 3.f;

	UFUNCTION()
	void DeathTimerFinished();

	bool bLeftGame = false;
	
	UPROPERTY(VisibleAnywhere)
	UTimelineComponent* DissolveTimeline;
	FOnTimelineFloat DissolveTrack;

	UFUNCTION()
	void UpdateDissolveMaterial(float DissolveValue);
	void StartDissolveMaterial();

	UPROPERTY(EditAnywhere, Category="Death")
	UCurveFloat* DissolveCurve;

	UPROPERTY(VisibleAnywhere, Category="Death")
	UMaterialInstanceDynamic* DynamicDissolveMaterialInstance;

	UPROPERTY(VisibleAnywhere, Category="Death")
	UMaterialInstance* DissolveMaterialInstance;

	/** Teams */
	UPROPERTY(EditAnywhere, Category="Team Colors")
	UMaterialInstance* RedDissolveMatInst;
	
	UPROPERTY(EditAnywhere, Category="Team Colors")
	UMaterialInstance* BlueDissolveMaterialInst;
	
	UPROPERTY(EditAnywhere, Category="Team Colors")
	UMaterialInstance* OriginalDissolveMaterialMaterialInst;
	
	UPROPERTY(EditAnywhere, Category="Team Colors")
	UMaterialInstance* RedMaterial;
	
	UPROPERTY(EditAnywhere, Category="Team Colors")
	UMaterialInstance* BlueMaterial;
	
	UPROPERTY(EditAnywhere, Category="Team Colors")
	UMaterialInstance* OriginalMaterial;
	
	
	UPROPERTY(EditAnywhere, Category="Death")
	UParticleSystem* DeathBotEffect;

	UPROPERTY(VisibleAnywhere, Category="Death")
	UParticleSystemComponent* DeathBotComponent;

	UPROPERTY(EditAnywhere, Category="Death")	
	class USoundCue* DeathBotSound;

	UPROPERTY(EditAnywhere, Category="Effects")
	class UNiagaraSystem* CrownSystem;

	UPROPERTY()
	class UNiagaraComponent* CrownComponent;

	UPROPERTY()
	class ABlasterPlayerState* BlasterPlayerState;

	UPROPERTY(VisibleAnywhere)
	USkeletalMeshComponent* AttachedGrenade;

	UPROPERTY(EditAnywhere, Category="Combat")
	TSubclassOf<AWeapon> DefaultWeaponClass;

	UPROPERTY()
	class ABlasterGameMode* BlasterGameMode;

public:
	void SetOverlappingWeapon(AWeapon* Weapon);

	bool IsWeaponEquipped() const;
	bool IsAiming() const;

	FORCEINLINE float GetAO_Yaw() const { return AO_Yaw; }
	FORCEINLINE float GetAO_Pitch() const { return AO_Pitch; }
	FORCEINLINE float GetHealth() const { return Health; }
	FORCEINLINE float GetShield() const { return Shield; }
	FORCEINLINE void SetHealth(const float Amount) { Health = Amount; }
	FORCEINLINE void SetShield(const float Amount) { Shield = Amount; }
	FORCEINLINE float GetMaxHealth() const { return MaxHealth; }
	FORCEINLINE float GetMaxShield() const { return MaxShield; }
	FORCEINLINE bool ShouldRotateRootBone() const { return bRotateRootBone; }
	FORCEINLINE bool IsDead() const { return bIsDead; }
	bool IsHoldingTheFlag() const ;
	FORCEINLINE UCombatComponent* GetCombatComp() const { return CombatComp; }
	FORCEINLINE UBuffComponent* GetBuffComponent() const { return Buff; }
	FORCEINLINE ULagCompensationComponent* GetLagCompensation() const { return LagCompensation; }
	FORCEINLINE ETurningInPlace GetTurningInPlace() const { return TurningInPlace; }
	FORCEINLINE UAnimMontage* GetReloadMontage() const { return ReloadMontage; }
	FORCEINLINE USkeletalMeshComponent* GetAttachedGrenade() const { return AttachedGrenade; }
	ECombatState GetCombatState() const;

	ETeam GetTeam();
	AWeapon* GetEquippedWeapon() const;

	FORCEINLINE UWidgetComponent* GetOverheadWidget() const { return OverheadWidget; }
	FORCEINLINE UCameraComponent* GetFollowCamera() const { return FollowCamera; }
	FVector GetHitTarget() const;

	void SetHoldingTheFlag (bool bHolding);
	bool IsLocallyReloading() const;
};
