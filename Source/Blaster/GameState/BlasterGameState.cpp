// Fill out your copyright notice in the Description page of Project Settings.


#include "BlasterGameState.h"

#include "Blaster/PlayerController/BlasterPlayerController.h"
#include "Blaster/PlayerStates/BlasterPlayerState.h"
#include "Net/UnrealNetwork.h"

void ABlasterGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABlasterGameState, TopScoringPlayers)
	DOREPLIFETIME(ABlasterGameState, RedTeamScore)
	DOREPLIFETIME(ABlasterGameState, BlueTeamScore)
}

void ABlasterGameState::UpdateTopScore(ABlasterPlayerState* ScoringPlayer)
{
	if (TopScoringPlayers.Num() == 0)
	{
		TopScoringPlayers.Add(ScoringPlayer);
		TopScore = ScoringPlayer->GetScore();
	}
	else if (ScoringPlayer->GetScore() == TopScore)
	{
		TopScoringPlayers.AddUnique(ScoringPlayer);
	}
	else if (ScoringPlayer->GetScore() > TopScore)
	{
		TopScoringPlayers.Empty();
		TopScoringPlayers.Add(ScoringPlayer);
		TopScore = ScoringPlayer->GetScore();
	}
}

void ABlasterGameState::RedTeamScores()
{
	++RedTeamScore;

	UpdateRedTeamScore();
}

void ABlasterGameState::OnRep_RedTeamScore() const
{
	UpdateRedTeamScore();
}

void ABlasterGameState::UpdateRedTeamScore() const
{
	if (ABlasterPlayerController* BlasterPlayerController = Cast<ABlasterPlayerController>(
		GetWorld()->GetFirstPlayerController()))
	{
		BlasterPlayerController->SetHudRedTeamScore(RedTeamScore);
	}
}

void ABlasterGameState::UpdateBlueTeamScore() const
{
	if (ABlasterPlayerController* BlasterPlayerController = Cast<ABlasterPlayerController>(
		GetWorld()->GetFirstPlayerController()))
	{
		BlasterPlayerController->SetHudBlueTeamScore(BlueTeamScore);
	}
}

void ABlasterGameState::BlueTeamScores()
{
	++BlueTeamScore;
	UpdateBlueTeamScore();
}

void ABlasterGameState::OnRep_BlueTeamScore() const
{
	UpdateBlueTeamScore();
}
