# About the project
<p>This is a multiplayer shooter game created with the objective of studying the various techniques used on multiplayer games to make players have a smooth gameplay experience.</p>

## How it was made
The project is divided in parts:
1. Creation of a multiplayer plugin to manage online sessions;  
2. Creation of a base project and installed to plugin on it;
3. Creation of the Playable Character class and added basic animations (movementation, jumps, etc) to it, the rest of the animations were created along the project;
4. Creation of the weapon base class;
5. Addition of the firing functionality to the weapon;
6. Addition of aiming functionality;
7. Creation of a simple Hud, with health and the weapon details;
8. Creation of match states to manage the match timers and initial gameplay setup;
9. Addition of more weapon types;
10. Creation of buffs and ammo pickups;
11. Creation of a Lag Compensation dedicated class to manage client lag;
12. Addition of player bookkepping;
13. Addition of Death announcement messages;
14. Addition of Headshots functionality;
15. Creation of the Teams Game Mode;
16. Creation of the Capture the Flag gamemode

## How to run it
<p> This project was made using Unreal Engine 5.1.0, to run it, you need this exact same version installed. When opened, the project will open up the MainMap.umap file, however, the initial screen for packaged games is the Default.umap file, this can be changed in the Project Settings. </p>

<p>When testing in the editor, make sure to set up the network mode to listen server, you can also simulate lag on the network preference settings, as the game was produced with lag optimization in mind.</p>

## The Maps
<p>There are three game maps: Main Game, Teams and Capture the Flag, a transition map and one Main Menu map (called Default map) </p>

<p>The default map is just a simple main menu, it has Host, Join, Settings and Quit button, and the Game Modes. When clicked Host or join, the players will be transfered to the Lobby map, once two or more players are on that map, the game will automatically travel to the selected game mode map and start the game. </p>

<p>**In the editor, it may not work as intended due to engine limitation, the best way to test it would be to open the map you want to check directly and press play.**</p>
