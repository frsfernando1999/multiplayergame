// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Interfaces/OnlineSessionInterface.h"

#include "Menu.generated.h"

/**
 * Data Flow - Host:
 * 1 - Call MultiplayerSessionsSubsystem->CreateSession() on Button Click
 * 2 - It calls CreateSessions() on the MultiplayerSessionsSubsystem
 * 3.1 - If it Succeeds, it calls UMultiplayerSessionsSubsystem::OnCreateSessionComplete
 * 3.2 - If it fails, it calls the menu delegate callback UMenu::OnCreateSession passing false
 * 4 - Then its bound delegate calls UMenu::OnCreateSession
 * 5 - After that, it changes the map to Lobby
 * 
 * Data Flow - Join:
 * 1 - Call MultiplayerSessionsSubsystem->FindSessions() on Button Click
 * 2 - It calls FindSessions() on the MultiplayerSessionsSubsystem
 * 3.1 - If it Succeeds, it calls UMultiplayerSessionsSubsystem::OnFindSessionComplete, which returns a filled TArray<FOnlineSessionSearchResult>
 * 3.2 - If it Succeeds, but the Array is empty, it broadcasts a failure to the main menu passing an empty array
 * 3.2 - If it fails, it calls the menu delegate callback UMenu::OnFindSessions passing an empty TArray and false
 * 4 - Then its bound delegate calls UMenu::OnFindSession passing a filled TArray and true
 * 5 - After that, it Filters the result and calls MultiplayerSessionsSubsystem->JoinSession passing the first session that matches the match type
 * 
 */

UCLASS()
class MULTIPLAYERSESSIONS_API UMenu : public UUserWidget
{
	GENERATED_BODY()
public:
	
	UFUNCTION(BlueprintCallable)
	void MenuSetup(FString LobbyPath = FString("/Game/Maps/Lobby"), int32 NumberOfPublicConnections = 4 ,FString TypeOfMatch = FString(TEXT("FreeForAll")));

protected:
	virtual bool Initialize() override;

	// Callbacks for the Custom Delegates on the MultiplayerSessionSubsystem

	UFUNCTION()
	void OnCreateSession(bool bWasSuccessful);

	void OnFindSessions(const TArray<FOnlineSessionSearchResult>& SessionResults, bool bWasSuccessful) const;

	void OnJoinSession(EOnJoinSessionCompleteResult::Type Result) const;

	UFUNCTION()
	void OnDestroySession(bool bWasSuccessful);

	UFUNCTION()
	void OnStartSession(bool bWasSuccessful);


private:
	// This Meta specifier MUST HAVE the exact same name as the button on the blueprint, else it'll crash
	UPROPERTY(meta=(BindWidget))
	class UButton* HostButton;

	UPROPERTY(meta=(BindWidget))
	UButton* JoinButton;

	UPROPERTY(meta=(BindWidget))
	UButton* SettingsButton;

	UPROPERTY(meta=(BindWidget))
	UButton* QuitButton;
	
	// UFUNCTIONS must be used for UE Callbacks
	UFUNCTION()
	void HostButtonClicked();

	UFUNCTION()
	void JoinButtonClicked();

	void MenuTeardown() const;

	class UMultiplayerSessionsSubsystem* MultiplayerSessionsSubsystem;

	UPROPERTY(BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	int32 NumPublicConnections{4};

	UPROPERTY(BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	FString MatchType{TEXT("FreeForAll")};
	FString PathToLobby{TEXT("")};
};
